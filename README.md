Sompyler
========

Sompyler is a software tool to generate sound and music that have both been defined in hierarchically
structured plain text-data (YAML or JSON). Focus of development is neither on audio quality nor on ease
of use for the normal by all means. Other available software synthesizers are better in that discipline
and should be preferred by musicians.

But for a nerd like me to study how sound and music actually "work", one does not need to purchase software
with a nice user interface and more comfort and bells and whistles than one can think of. A nerd prefers to do it 
from the very scratch, even avoiding use of foreign code, i.e. software plug-ins and modules, that have been
written explicitly with musicians in mind. The so-called "NIH" syndrome, abbreviation of "not invented here", denoting
re-invention of known things like the wheel just for the sake of avoidance of re-use of what is there,
but then for sake of knowing and understanding these things really well, is key for learning by programing and
is actually not a syndrome at all.

In the first place Sompyler or that way of making music in general (that is called "Neusik" by the way, from
"NeRDMusik", with RDM alone as abbreviation for *Radically descriptive model*) is a fascinating kind of supplement for my limited
capability to play the piano or other instruments really "in time", which is due to movement disorders. So it is primarily
individual software, written not for others, just for myself. Everyone feel free to use it, however, who is inclined
to make sound and music from text. Of course, that is a nerd's way of doing things, rather than a musician's.  

Following chart shows how it basically works:

```mermaid
graph TD
    Sompyler -->|1. reads YAML| Sc
    Sompyler -->|2. reads YAML| In
    Sc[Score file] -->|first section| Stage[Stereo stage]
    Stage -->| defines set of | Voice 
    Voice -->| references | In[Instrument]
    Sc -->|following sections| M[Measures]
    M -->|include metadata and| VM[Voice-bound measure]
    VM --> Note
    Note -->| 5. is associated to matching cached| So[Sound]
    Note -->| 3. passed to | In
    In -->| 4. synthesizes | So
    So --> As(Assembler)
    As --> Au[Audiofile]
    Au -->|may be readily imported into| DAW[Your DAW]
    Au --> P[Your fav. player]
    P -->| 7a. listen & fix in text editor | Sc
    P -->| 7b. listen & improve in text editor | In
    Sompyler -->| 6. second pass | As
```
First, the Sompyler reads in a score file (conventional file ending \*.spls) which initially
references instrument definition files, among other metadata, then describes every measure
of the piece as a separate YAML document in a stream. Each instrument definition describes in a compact
text format mostly by bézier curve coordinates, as well YAML-compliant, exactly how notes sound,
to whatever extend depending on their properties (pitch, intensity, length, plus custom ones).

The calculated PCM samples are written into an audio file that can be played with your
favourite audio-player or it can be imported in DAW tools for further processing.

Sompyler is incapable of real-time music production, and it lacks by intention
a graphical user interface. Rather, it works more like a traditional unix tool, the only "widget" is
a progress bar. As PCM samples in WAV format, Sompyler output can be easily imported into DAW software.

By the way, single sounds can be rendered with `scripts/render-single-sound`. That tool has also a mode
for deep and comparative sound analysis. However, the development of this mode is paused.




An example
----------

First we define an instrument which basically consists of metadata like a name and, most importantly, its character
that defines what you will hear when the instrument plays a note. Then we write a tiny piece using
that instrument.

NOTE: YAML format supports "# comments" that we use to explain what we do here. The parser
will ignore everything from "# " to the line ending. 

    #!/bin/sh
    ###################################################################################################
    # cd $SOMPYLER_DIR # Please replace by the directory wherever you installed sompyler into.
    cat > lib/instruments/kliber.spli <<END
    # A, O, R, S, PROFILE etc. can be mapped as sub-attributes to "character", then you can aside of it
    # name the instrument (name: Flute in B) and also provide other metadata to your liking.
    A: 0.05:1,2;2,9 # Attack (50ms), always required.
    O: sine         # Oscillator, omittable as "sine" is default.
                    # Don't forget to try sawtooth, square, triangle, slopy or noise
    R: 0.2:1;1,0    # release (200ms), omittable (ignored) if A or S ends with 0
                    # Length is ignored when it comes to note length, which is how legato is
                    # realized.
    S: 3:4;1,1      # decay + sustain (3s), omittable if A ends with 0 or you are okay with constance
                    # After the length value in seconds and ":", the initial relative value is
                    # indicated. It will be implicitly scaled so that it equals to attack end = 9.
                    # After the semi-colon, further edge coordinates of the enveloping polygone
                    # around the bezier curve (polynom) to be calculated accordingly
    PROFILE: [100, 90, 89, 75, 81, 71, 65, 67, 59, 55, 38]
          # Volume of 1st, 2nd etc. overtone
          # i.e. the volume of it, in dB% ("100dB" = playback volume)
    END
    cat > scores/example-piece.spls <<END # best mkdir a sompyler scores directory in your ~/
    title: Sompyler example piece
    license: Just let it be yours ;) # Too trivial for CC indeed
    stage:
         kliber: 1|1 0 kliber
         # "kliber" voice name referenced below
         # "1|" position from left border of hearing scope
         # "|1" position from right border of hearing scope
         # "0" distance from listener
         # "kliber" path to instrument definition, ".spli" is implied
         # path is read as relative to either of
         #   - $sompyler/lib/instruments
         #   - directory of this file
    ---
    _meta:
         stress_pattern: 2,0,1,0 # first beat full stress, third half stress
         ticks_per_minute: 120   # unit cf. stress_pattern
         upper_stress_bound: 100 # full stress = 100% volume (linear scale)
         lower_stress_bound: 94  # unstressed beats at half level (-6dB)
    kliber:
        0:       # by the way, this is the offset tick, counting starts at 0
        - C4 1   # and "1" signifies the number of ticks how long the note must be played
        - G3 4   # Length of a tick is defined by ticks_per_minute that can be also e.g.
        1: D4 1  # 100-120 so the tempo accelerates or be a shape of whatever form
        2: E4 2  # (same notation like A/S/R shapes in instrument definition, but
                 # without length.)
    
    ---
    # "_meta" is implicitly taken from last measure where it has been defined
    # Let's write denser:
    # (Quotes left, with them applied it would be actually JSON, a YAML subset indeed).
    kliber: {
        0: [G4 2, D4 2, B3 1], 2: [F4 1, C4 2, A3 1],
        3: [g 1, e 1] # German tone names are supported, too
                      # (they are shorter on this octave)
                      # Scale definition and tone names s. lib/tones_euro_de+en.splt
    }
    END
    scripts/sompyle scores/example-piece.spls /tmp/example.wav
    aplay /tmp/example.wav # or any other audio player of your choice
    ###################################################################################################

    
Prerequisites
-------------

### User

Know-how and ability to edit raw text files with a powerful text editor. There will be no
"official" user interface that hides the text definition stuff behind eye-candy
widgets. Sompyler works according to the pure Unix philosophy, i.e. reasonable text-data in,
audio (PCM) data out, everything else is not in my scope.

### Hardware

To performantly generate music with Sompyler, you should have a strong multi-core CPU,
as for each core a separate synthesizing worker is fed from the queue of score's notes.
Even though hard-disk cache is used for the distinct notes, observe the length of each
note in respect to any hardware limits. The notes are finally assembled all in RAM
memory (for each channel separately, so take them twice for stereo), from where the
audio file is written. Have that in mind if you want to make long-playing pieces.

Dedicated sound production hardware is not required, but you might want to have it
nevertheless to edit the sound output for reverb and alike.

Dedicated high quality audio playback hardware is recommended so a bad listening
experience is likely to result from errors in the definition you need to correct,
instead of poor quality of the electronic devices used.

### Software

Sompyler has very few dependencies:

  * It is written in Python 3, hence it needs that interpreter to run.
  * PyYAML – to parse YAML or JSON text
  * soundfile – transforms the calculated raw samples into a file, e.g. FLAC
  * Numpy – sample and other calculations on the bare-metal level

If you want to use scripts/render-single-sound to analyse sound visually:
  * matplotlib

As an additional challenge for myself (and please have that in mind when you consider
contribution), Sompyler deliberately does *not* depend on any foreign code which
explicitly targets music people. Hence I can claim Sompyler is original sound and
music software. No MIDI stuff is required to be installed, no soundfonts are needed.


Installation
------------

Install the prerequisite python3 modules above or refer to the requirements.txt file in
the distribution. You may want to install them in a bunch with `pip install -r requirements.txt`
in a dedicated virtualenv.

These requirements have their own again to work properly. In my Debian system (Buster), I needed
to run `apt-get install build-essential python3-dev libsndfile1`. For your system, if it is
different, please refer to your manuals / online resources or contact your system administrator.

Then you may want to install python3 cython, a C compiler and essential libs (done with above command on Debian systems) and run once
`python3 Sompyler/synthesizer/shape/cythonize-bezier-gradient.py build_ext --inplace`.
On the author's dual-core machine, the performance gain by that was quite impressive when
he sompyled his "Anfängerstück 1", a creative commons work included in [the text-music
repository](https://gitlab.com/flowdy/text-music):

            without     with optimization
    real    24m10,496s  5m20,290s
    user    47m38,636s  10m20,596s
    sys     0m7,704s    0m7,024s

So, optimization by pre-compiled bézier gradient calculation lead to over 450% faster runs
as compared to the purely-python default.


Copyright & License
-------------------

(C) 2017 Florian 'flowdy' Heß

See LICENSE containing the General public license, version 3.

Sompyler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sompyler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sompyler. If not, see <http://www.gnu.org/licenses/>.
