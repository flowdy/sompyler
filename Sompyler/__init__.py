import sys, os
from . import synthesizer
from .audiofile import save
from Sompyler.orchestra import play, NoteRenderingFailure

def FROM_BASE_DIR(*args, ending=None, prefer_directory=None):
    *dirpath, fpath = args
    if '.' not in fpath and ending is not None:
        fpath = '.'.join((fpath, ending))
    if fpath[0] in './' and 'SOMPYLER_LIMITS' not in os.environ:
        # We need to block potential attackers from gaining access to external files.
        return fpath
    if '../' in fpath:
        raise FileNotFoundError(fpath) # such files are always not found even if
                                       # they exist. Security reasons, too.
    if prefer_directory is not None and os.path.isfile(
            (fullpath := os.path.join(prefer_directory, fpath))
        ):
        return fullpath
    else:
        return os.path.join(os.path.dirname(__file__), "..", *dirpath, fpath)


class Progress:

    def new_note(self, note_id, length_s, position, description):
        pass

    def rendered_note(self, note_id, length_s):
        pass

    def reuse_note(self, note_id):
        pass

    def retune_notes(self, htonic):
        pass

    def reuse_former_note(self, note_id):
        pass

    def delete_orphan_notes(self, count):
        pass

    def next_measure(self, number, cumlength):
        pass

    def assemble(self, total_length):
        pass

    def switch_to_room_reverb_assembler_progress(self):
        pass

    def apply_tone(self, note_id, position, offsets):
        pass

    def report_cache_directory(self, cache_dir):
        pass

    def emit_premidi_note(
            self, abstime, channel, keynum, length, stress, comment=None,
            **further
        ):
        pass

    def emit_premidi_comment(self, comment, **further_opts):
        args = 5 * [None]
        if 'abstime' in further_opts:
            args[0] = further_opts.pop('abstime')
        if 'voice' in further_opts:
            args[1] = further_opts.pop('voice')
        if 'keynum' in further_opts:
            args[2] = further_opts.pop('keynum')
        return self.emit_premidi_note(
                *args,
                comment=comment,
                **further_opts
            )

def process(score_file, *, workers, monitor, subst_instruments,
            samplerate=None, samplewidth=None, out_format=None, out_file=None,
            room=None,
            volume=1
        ):

    if monitor is None:
        monitor = Progress()

    if samplerate:
        synthesizer.SAMPLING_RATE = samplerate
    if samplewidth:
        synthesizer.BYTES_PER_CHANNEL = samplewidth

    samples = None
    try:
        samples = play(
            score_file, monitor, workers,
            substitute_instruments=subst_instruments,
            room=room
        )
    finally:
        monitor.assemble( samples.shape[0] if samples is not None else 0 )

    if volume:
        synthesizer.normalize_amplitude(samples, volume)

    save(samples, out_file, out_format)

    return 0

