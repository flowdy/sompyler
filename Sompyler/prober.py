from Sompyler.synthesizer.sound_generator import SoundGenerator

from Sompyler.synthesizer.sympartial import Sympartial

sound = None
resolution = None


def init(s, r):
    global sound, resolution
    sound = s
    resolution = r * round(len(sound) / sum(r))


class SoundProber(SoundGenerator):

    def find_energies(
            self, symp, number, minfreq, maxfreq, resolution,
            min_freq_distance=0.001
        ):
        best = []
        to_scan = [(None,0)] * number
        to_scan[0] = (((minfreq, maxfreq), -10000), 0)

        def central_energy(minfreq, maxfreq):
            freq = (minfreq + maxfreq) / 2
            return (minfreq, maxfreq), symp.probe(
                        freq, lambda *_: 1, 1, resolution
                    ).cleared_volume

        def scan_later(region, cmpvalue):
            to_scan.append((region, (region[0][1] - region[0][0]) / cmpvalue))
            to_scan.sort(key=lambda x: x[1], reverse=True)
            to_scan.pop()

        while to_scan:
            (mybest, _), *to_scan = to_scan

            print("Find more cleared volume in halfs between "
                 "{}Hz and {}Hz ...".format(*mybest[0])
                )

            while True:
                (minfreq, maxfreq), clvolume = mybest

                if (maxfreq - minfreq) < min_freq_distance:
                    break

                checked_freq = (minfreq + maxfreq) / 2
                lower, higher = sorted((
                            central_energy(minfreq, checked_freq),
                            central_energy(checked_freq, maxfreq)
                        ), key=lambda x: x[1]
                    )

                # if higher[1] > clvolume:
                mybest = higher
                # else:
                #     raise RuntimeError(
                #             "higher does not exceed current best: {} vs. {}"
                #             .format(higher[1], clvolume)
                #         )

                scan_later(lower, higher[1] - lower[1])

            print("Found partial at {}Hz".format(
                mybest[0][0]
            ))

            best.append(mybest)

        return dict( (b[0][0], b[1]) for b in best )

    def render(self, *args, **kwargs):

        partial_ff = 0
        base_freq = args[0]
        symp = ProbingSympartial()

        def find_energies(times, partials, mincent, maxcent):
            nonlocal partial_ff
            for _ in range(times):
                minfreq = base_freq * (partial_ff + 2 ** (mincent/1200))
                maxfreq = base_freq * (partial_ff + 2 ** (maxcent/1200))
                yield from self.find_energies(
                        symp, partials, minfreq, maxfreq
                    ).keys()
                partial_ff += 1

        scan_profile_ranges = kwargs.pop('scan_profile_ranges', None)
        if scan_profile_ranges:
            freqs = list(chain.from_iterable([
                    find_energies(*spr) for spr in scan_profile_ranges
                ]))
            freqs.sort()
            kwargs['force_freqs'] = freqs

        return self.prepared_iter(*args, **kwargs)


class ProbingSympartial(Sympartial):

    _ProbeResult = namedtuple('Result', ('value', 'count', 'first'))
    _NDProbe = namedtuple('NDProbe', ('norm', 'deviation'))
    _ProbeRecord = namedtuple('ProbeRecord', (
        'norm', 'dev', 'cleared_volume', 'ideal_env', 'diff_dB',
        'stability', 'minimum', 'maximum',
        'inarea', 'overtop', 'underbottom', 'afterend'
    ))
    _ProbeRecord.__str__ = lambda o: (
            "St {o.stability.norm:.2f} {o.stability.deviation:.2f}"
            "; Max {o.maximum.norm.value:.1f}("
                "{o.maximum.norm.count},{o.maximum.norm.first})"
                " {o.maximum.deviation.value:.1f}("
                "{o.maximum.deviation.count},{o.maximum.deviation.first})"
            "; Min {o.minimum.norm.value:.1f}("
                "{o.minimum.norm.count},{o.minimum.norm.first})"
                " {o.minimum.deviation.value:.1f}("
                "{o.minimum.deviation.count},{o.minimum.deviation.first})"
            "; Ina {o.inarea.norm.value:.1f}("
                "{o.inarea.norm.count},{o.inarea.norm.first})"
                " {o.inarea.deviation.value:.1f}("
                "{o.inarea.deviation.count},{o.inarea.deviation.first})"
            "; Ovt {o.overtop.norm.value:.1f}("
                "{o.overtop.norm.count},{o.overtop.norm.first})"
                " {o.overtop.deviation.value:.1f}("
                "{o.overtop.deviation.count},{o.overtop.deviation.first})"
            "; Unb {o.underbottom.norm.value:.1f}("
                "{o.underbottom.norm.count},{o.underbottom.norm.first})"
                " {o.underbottom.deviation.value:.1f}("
                "{o.underbottom.deviation.count}"
                ",{o.underbottom.deviation.first})"
            "; Aft {o.afterend.norm.value:.1f}("
                "{o.afterend.norm.count},{o.afterend.norm.first})"
                " {o.afterend.deviation.value:.1f}("
                "{o.afterend.deviation.count},{o.afterend.deviation.first})"
        ).format(o=o)

    def render(self, freq, share_func, duration, **args):

        phase = args.pop('phase', 0)

        def segments(array):
            out = []
            position = 0
            for sps in samples_per_segment:
                sps = int(sps)
                out.append(np.average(array[position:position+sps]))
                position += sps
            return np.array(out)

        fmin = np.finfo(float).eps
        def linear_to_dB(value):
            return 20*np.log10(np.where(value <= fmin, fmin, value))

        def indics(array):
            nz = np.nonzero(array)[0]
            return Sympartial._ProbeResult(
                    value=sum(array),
                    count=len(nz),
                    first=nz[0] if len(nz) else None
                )

        def xindics(array, op):
            v = op(array)
            p = np.nonzero( np.where( array==v, array, 0 ) )[0]
            return Sympartial._ProbeResult(
                    # get dB-scaled absolute amplitude value
                    value=linear_to_dB(abs(v)),
                    count=len(p),
                    first=p[0] if len(p) else None
                )

        def probe(norm, dev, op):
            return Sympartial._NDProbe( norm=op(norm), deviation=op(dev) )

        share, _ = self.normalized_env(
                share_func, duration, args
            )

        if not callable(share_func):
            if not share_func:
                share_func = 0
            share_func = lambda x: share_func

        elen = len(share)
        share = log_to_linear( share * share_func(elen) )

        slen = len(sound)

        # Equalize probe's envelope and sound length
        if slen > elen:
            share = np.append(share, np.zeros(slen-elen))
            elen = slen
            olen = len(resolution)
        elif elen > slen:
            underlength_part = sum(resolution) * slen/elen
            olen = 0
            for i in np.cumsum(resolution):
                olen += 1
                if i > underlength_part:
                    break
            sound = np.append(sound, np.zeros(elen-slen))
            slen = elen

        if isinstance(resolution, (list, np.ndarray)):
            samples_per_segment = resolution
        else:
            samples_per_segment = (
                    [int(slen / resolution + 0.5)] * resolution
                )

        env_div = np.where(share == 0, 1, share) # protection against
                                                 # division by zero

        iseq = np.arange(len(share))
        oscillator = self.oscillator

        # level out amplitude modulation, if any
        ampl_mod = self.oscillator.amplitude_modulation
        if ampl_mod:
            share *= ampl_mod.modulate(iseq, freq)
            oscillator.amplitude_modulation = None
            
        # Division by RMS leads to mean values around 1 instead of RMS
        single_period = oscillator(freq, int(SAMPLING_RATE/freq))
        rms = np.sum(single_period**2) / len(single_period)
            # reverted sqrt()

        sin_probe = sound * oscillator(
                freq, iseq, phase+0, **args
            ) / (env_div*rms)
        cos_probe = sound * oscillator(
                freq, iseq, phase+90, **args
            ) / (env_div*rms)

        oscillator.amplitude_modulation = ampl_mod

        # Even out phase to the average (ears do that as well)
        amplitude = np.hypot( sin_probe, cos_probe )
        mean_angle = np.mean(np.arctan2(cos_probe,sin_probe)) 
        norm_angle = np.arctan2( cos_probe, sin_probe ) - mean_angle
        norm_probe = segments(np.cos(norm_angle) * amplitude)
        dev_probe = segments(np.sin(norm_angle) * amplitude)
        # We need to renormalize normal and deviative phase
        # to the amplitude so we can staple the bars without
        # distorting it.
        ampl = np.hypot(norm_probe, dev_probe)
        probesum = norm_probe + dev_probe
        probesum = np.where(probesum == 0, 1, probesum)
        scaled_norm_probe = norm_probe / probesum * ampl
        scaled_dev_probe = dev_probe / probesum * ampl

        return self._ProbeRecord(
                norm=scaled_norm_probe,
                dev=scaled_dev_probe,
                cleared_volume=sum(scaled_norm_probe) - sum(
                        np.absolute(scaled_dev_probe)
                    ),
                ideal_env = segments(share / env_div),
                diff_dB=linear_to_dB(scaled_norm_probe + scaled_dev_probe),
                stability=probe(norm_probe, dev_probe,
                        lambda array:
                            resolution
                          / (1 + sum(np.absolute(np.diff(array))))
                    ),

                maximum=probe(norm_probe, dev_probe,
                        lambda i: xindics(i, np.amax),
                    ),

                minimum=probe(norm_probe, dev_probe,
                        lambda i: xindics(i, np.amin),
                    ),

                inarea=probe(norm_probe, dev_probe,
                        lambda array: indics(np.where(
                            (0 <= array[:olen]) & (array[:olen] <= 1),
                            array[:olen], 0
                        ))
                    ),

                overtop=probe(norm_probe, dev_probe,
                        lambda array: indics(np.where(
                            array[:olen] > 1, array[:olen]-1, 0
                        ))
                    ),

                underbottom=probe(norm_probe, dev_probe,
                        lambda array: indics(np.where(
                            array[:olen] < 0, array[:olen]*-1, 0
                        ))
                    ),

                afterend=probe(norm_probe, dev_probe,
                        lambda array: indics(array[olen:])
                    ),

            )

