from . import synthesizer
import soundfile

FORMAT_SPECS = {
    "ogg": { 'format': "OGG", 'subtype': "VORBIS" },
    "wav": { 'format': "WAV", 'subtype': "FLOAT"  },
}

def save(samples, out_file, out_format=None):

    try:
        name = out_file.name
    except AttributeError:
        name = out_file

    ext = name.rsplit(".", 1)[1]
    if ext == 'npy':
        import numpy
        numpy.save(out_file, samples)
    else:
        CHUNK = 2**16
        try:
            CHANNELS = samples.shape[1]
        except IndexError:
            CHANNELS = 1
        with soundfile.SoundFile(
                out_file, "w", samplerate=synthesizer.SAMPLING_RATE,
                channels=CHANNELS, **FORMAT_SPECS.get(ext, {})
            ) as sf:
            numsamples = samples.size
            index = 0
            while index <= numsamples:
                sf.write(samples[index:index+CHUNK])
                index += CHUNK

