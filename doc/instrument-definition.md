Instrument specification
========================


An instrument definition consists of one or more variation.

A variation yields, i.e. selects or constructs, a sound generator to be used to
render a tone, depending on certain properties of a note. Which is the decision
of the instrument designing user.

A sound generator consists of at least one layer. A layer has a frequency
factor and a volume, that is, a maximum energy on that frequency.


Sympartial specification
------------------------

A sympartial is a partial, an elementary oscillation of a single initial
frequency, that may be accompanied by further partials realized by modulation.
You can define frequency modulation, amplitude modulation, and wave shape.

A sympartial has an A(D)SR envelope that consists of up to for bézier shapes:

  * Attack: Length of this phase is always fixed.

  * Sustain: Phase is cropped or extended depending on how long the tone is.
    Extension considers the rise between the last two coordinates.

  * Tail: Optionally defines re-rise of sustain reduced below an indicated
    level over sustain time. Seldom used, but may come in handy e.g. for
    articulatory sounds that have a plosive end.

  * Release: Length and vertical start (maximum) is adjusted to the amplitude
    at the factual end of the tone's sustain. Length of the release phase is
    not considered part of the defining tone length, so tones can overlap.
    That way, legato can be realized.

Percussive sympartials only have an attack phase that ends by zero. With
undefined sustain, partial is extended by a constant sustain if attack
and release cannot fill the required length alone. A sustain ending at 0
causes a release phase, if any is defined, to be omitted.

There is no special "decay" phase. Shape attack or sustain/release
appropriately. A decay is defined in attack or sustain or in both, depending
on how much of it must be independent from requested tone length.

Every phase is specified by length (seconds), maximum volume share at start,
and followed by the shares of consecutive points up to the end, defined by
semicolon-separated "x,y"-coordinates describing the polygone-defined
envelope of the bézier curve.

The x-dimension of these points is automatically normalized to the length of
the shape, so only the ratios of the x-positions count. The y-part value is
normalized to the start value = 1, expressed however by inverted dBFS scale:

    100 %dB =    0dBFS
      0 %dB = -100dBFS

If the start value is 0 or omitted at all, the amplitude values are normalized
to the maximum value of the shape.


### Example:

    1.2:100;1,50;3,0

In this example, the rise is not linear, but is falling in a decreasing extent.
By -50 of -100dB in the first 0.4 sec, that is slowly reduced to -25 of
-100dB in the last.


### Modulations

 * `5;1:100+45` = 5Hz, sine-modulated part relates to constant part in a ratio
   of 1:100, phase angle 45 degrees.

 * `5f;...` 5 times the base frequency of the tone.

 * `5F;...` 5 times the carrier frequency.

 * `5f@sine;...` modulation oscillates in sine form (which is the default).
   Instead of sine, you can reference any other primitive or user-defined
   oscillator by name. If user-defined, any modulations to the referenced
   oscillator will persist. That way, you can modulate with arbitrarily complex
   oscillations.

 * `[...];...` Before the semicolon, you can indicate a modulation envelope
   shape inside a pair of brackets. Make sure to not omit the length, it will
   be ignored.

The tag for frequency modulation is "FM", the tag for amplitude modulation is
"AM".


### Waveshaping

You can define the wave shape the same way as you define attack etc., but omit
the length.

The shape you define is actually the increasing flank of the result wave. The
y-parts of the rendered form points are mapped to the x-parts of rendered the
wave shape, and the values get replaced by the corresponding y-values of the
wave shape.

Please specify a start value, and the first coordinate with x=0.
A coordinate with y equal to start value will be mapped to the middle line of
the wave.

That shape relates to the full height and will be mirrored to the slope, so
the whole wave period remains symmetric.

The sharper the edges are in the curve, the more dominance will get the higher
overtones compared to the lower.


Character definition
--------------------

The essential part of an instrument definition is its "character" that
determines how it actually sounds. In its most complex form, it can adapt
to every aspect of a note. Have a glance at `instruments/dev/piano.spli`
for a more sophisticated level.

There are two or three mandatory properties of the character to define at
least. "name" and "source" are merely informational:

    name: My first instrument
    source: None, really. Just testing.
    character:
        O: sine     # one of "sine", "square", "sawtooth" or "noise"
        A: ...      # attack shape definition, see above
        R: ...      # define release phase if attack ends in ...,n with n>0

With that, you just defined an instrument emitting exactly one frequency.

Properties "S", "AM", "FM", and "WS" are optional. Define a modulation
with "nf;...", given n≤1, to get overtones.

Such a simple instrument can easily be developped using
`scripts/render-single-sound`.

Do you want more dynamic, richer sound with multiple separately controlled
overtones? Be welcome and enhance your pleasure with SPREAD, PROFILE and TIMBRE.


### PROFILE

Manages properties of layers, i.e. sympartials with a volume and a defined
frequency relative to the requested base frequency. Do not mistake layers for
partials however, as sympartials with modulations and/or a wave-shape defined
can each produce a bunch of partials.

Lacking properties are completed by interpolating the respective properties
from neighbouring layers (regarding their actual ordinal number), using
the corresponding property of the base properties defined next to a profile,
or an upper-level variation if there is any defined – in that order.

    character:
        O: sine
        R: ... # used for all partials without release definition
        PROFILE:
            - A: ... # fundamental layer, attack definition
              V: 100 # mandatory volume of fundamental
              # R: (omitted because defined for all)
            - A: ... # 1st overtone
              V: 89
            - ...    # how many layers you like
            

An additional property for volume: "V" or "adj\_volume". The unit
is %dB. You should stick to the range 0-100. 100%dB represent the maximum
volume of the audio system playing back, so 0%dB is -100dBFS and 100%dB is
0dBFS.

It is merely relative and depends on settings on operating system and
user-land level.

If VOLUMES shape applies in connection with a profile, V i.e. adj\_volume
is optional and indicates the difference to what VOLUMES indicates.

An optional property is "D" or "deviance": It specifies the up- or downshift
of frequency relative to the partial's position in the harmonic series varied
by SPREAD (s.b.) if there is any is defined.

PROFILE or any sub-item can be an list of integers. As such, the integers
indicate the volumes. All other properties are inherited and interpolated from
above levels of the definition.

A dictionary-type item can have a property "match", that can be an integer
specifying the ordinal partial number or a string in format "Nn[+/-]N", eg.
3n-2 for partials 1, 4, 7 and so forth. Only for these partials the indicated
properties are applied.

As many layers, i.e. their ordinal number are probed to be matched by every
item, but no further than all items could match at least once. So, for instance,
if you happen to define "match: 1000", a thousand partials would be rendered
theoretically, but in practice layers beyond Nyquist frequency will not be
rendered.


### MORPH

Similar to profile, however with following difference:

    * Items are strings of format "MATCH SHAPE", that is a match and a shape,
      the latter with or without length indicated, and concatenated by single
      space.

    * The specified length is evaluated as a weight actually. So, if a partial
      number is matched by more than a single item's partial number pattern,
      their shapes are meaned according to these weights.

    * The shapes are applied by multiplication with the overall rendered
      envelope for that sympartial. So there is no more a difference
      between attack, sustain and release.


### VOLUMES

You can make the volumes of layers *optional*, i.e. *additional*, by
defining of a "VOLUMES" shape.

The shape determines the base volumes of the partials. For all layers, the sum
of the energy for its ordinal number defined in "VOLUMES", plus the positive or
negative energy specified for its ordinal number in the "PROFILE", will be
applied.


### TIMBRE

Just another shape definition. The x-axis of the shape is mapped to the partial
frequency. The y-axis indicates the factors which multiply (or rather reduce,
because they are ≤1) the PROFILE volumes for the corresponding partials
depending on their actual frequency.

To view the graph of a timbre, just use
    `scripts/render-single-sound -S "20000:..."`.
Make sure, optional dependency `matplotlib` is installed, because the timbre is
not a sound you can listen too, it is rather a function with a custom shape.

Coordinates are not edges by default. Edges in the curve can be defined by an
'!' after the x,y notation. (This is a general feature, not restricted to
features. Feel free to try it for envelope shapes.)


### SPREAD

Array of partial frequency distances in cent unit. A cent unit is 1/1200 of an
octave. Instead of an array, it can also be defined as a shape.

Please note that every value implicitly stacks onto the preceding values, and
the array is cropped or extended by zeroes depending on PROFILE length.


### RAILSBACK

A shape or a list of 88 items for the 88 keys of a standard piano, indicating
the pitch deviation to tune the instrument. Needed in the root variation when
you define sounds with less prominent first partials.


### Labels and reference

If a non-property key is given among properties, it is expected to be the label
of a property group. The key must be a string other than the specially handled
that have been described earlier. A labelled property group can only contain
sympartial properties.

Wherever you can set a property value, it can be "inherited" indirectly from
the property group the label of which is expected after the distinctive "@"
sign. The relationship to the referenced property group must be sibling of
current or of an upper level. Circular references are detected.

Modulation definitions can have label references, too. The oscillator of the
referenced property group (or "protopartial" if you prefer) is used for the
wave form of the modulation.


### Structured variations

To shape a sound depending on the requested pitch of the tone, you can define
a timbre. So you have a smooth gradual change of partial volume shares.

But you cannot define changes to e.g. attack in this way.

To do that, use variations instead. Just add ATTR and TYPE properties to your
definition, and further properties the keys of which must be either an integer,
a float or a string with a preceding "=" sign, so they can be distinguished
from labels.

As a value of such a property ("variation"), you can define all sympartial
properties, TIMBRE, PROFILE, and nested variations as well. Last but not least,
you can define new and redefine existing labelled property groups from
hierarchy above. SPREAD is generally not a good idea to use in variations,
though. Chords would contain ugly beats if component tones use different
SPREADs.

ATTR must name a note property that is evaluated to choose the variation:
"pitch" | "stress" | "length" or any custom ones.

TYPE must be set to one of the following values, or the software tries to infer
it from the type of ATTR value or the type of the keys:

  * "merge" – keys of given variations are edges between which is
    interpolated in a linear fashion. Say, you define variations for pitches
    400Hz and 800Hz, then a given request of pitch 600Hz would lead to an
    equal interpolation in the middle between these values.

  * "stack" – matching variations add up their properties in the sequence
    defined in ATTR. Where properties have been defined according their
    keys, replacement takes place.

  * "next" – the next matching variation is selected.

By using variations, you should be able to approach the sounding character
of physical instruments in any extent of detail you wish. For instance,
the keys of a piano all have their distinct sounding beside the pitch.
Just compare A0 and C8, i.e. the left- and the rightmost key. The right one
is much shorter and more percussive than the left.


Where to store the instrument definition
----------------------------------------

Store the instrument file with the conventional ending `*.spls`.

When you indicate the path of an instrument definition in a score file,
initial document (if you use YAML syntax), `stage` slot, omit `.spls`.

The software tries to resolve the reference as a path only if it
contains at least one slash. If it starts with a slash, it is interpreted
as absolute. If it starts with a dot, it is interpreted as relative to
the current working directory in which `sompyle` is executed. If it starts
directly with a path segment, e.g. "my\_instruments", that directory
is expected either in the directory where the score file lies, or in
the Sompyler installation directory, precisely in `$SOMPYLER/lib/instruments`.

In that directory, "dev/" is included for a start, containing instrument
definitions to approach some well-known instruments like a piano, or flute.
Do not expect a good quality or even recognizability, they have been
deduced from physical and acoustical descriptions in the literature.

If the name reference does not contain a slash or the file ending `.spls`,
say `foo` for instance, a slot `instrument foo` is expected in the initial
document of the YAML stream (respectively the JSON document). This is an
inlined instrument definition. To avoid having all tones re-rendered in
each run, no matter if you changed them or not, indicate

    NOT_CHANGED_SINCE: YYYY-MM-DD HH:MM:SS

of course with numerical year, month, etc. replaced. It substitutes the mtime
timestamp of a file upon which is decided if a tone actually needs to be
re-rendered.
