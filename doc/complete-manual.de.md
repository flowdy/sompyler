Sompyler – Offline-Synthesizer, textgesteuert
=============================================


This is a complete manual (draft) in German, native language of the creator
of Sompyler. A translation back to English is planned once the contents are
reader-proven.

Inhalt
------

  1. Vorwort
  2. Was ist Sompyler, was nicht?

      1. Python
      2. Eingabeverarbeitung: YAML/JSON
      3. Performante Berechnungen mit Massendaten
      4. Soundformatierung

  3. Wie kann Sompyler ausprobiert werden?

      1. Lokale Installation
      2. Online-Demo

  4. Wie sieht eine Sompyler-Score-Datei aus?
  5. Klangbeschreibungen (TODO)

      1. O: Oszillatoren
      2. A, S, R, FV, WS: Verläufe
      3. FM, AM: Modulation
      4. PROFILE: Mehrere Layer
      5. MORPH: Gesamt-Einhüllende, ADSR-Segmenten übergeordnet
      6. SPREAD: Abweichungen von der natürlichen Obertonreihe
      7. TIMBRE: Resonanz und Formantengestaltung
      8. RAILSBACK: Oktavspreizung und -stauchung
      9. Variationen
      10. Benannte und referenzierbare Gruppen von Eigenschaften

  7. Beschreibung der Stimmen auf der gedachten Bühne (`stage`)

      1. Position auf der Bühne: Richtung und Distanz
      2. Instrument: Via Pfad referenziert oder eingebettete Klangbeschreibung?
      3. Artikel: Tonbezogene Aktivierung von Klangeigenschaften

  8. Spezifikation der Stimmung (`tuning`)

  9. Spezifikation der Eigenschaften des Raumhalls (`experimental room`)

  6. Taktbeschreibungen (TODO)
 
      1. `_meta`: Eigenschaften des Taktes

	      1. `stress_pattern`: Betonungsabfolge
          2. `beats_per_minute` oder `ticks_per_minute`: das Tempo
          3. `upper_stress_bound` und `lower_stress_bound`: Betonungsbereich
          4. `cut`
          5. `repeat_unmentioned_voices`
          6. `is_last`

      2. für jede Stimme: `_meta`
      3. Noten und Akkorde
      4. Kettennotation
      5. Untertakte und Folgetakte


Vorwort
-------

In den Anfangsjahren der Musiksynthese, als Hardware noch sehr teuer war und
in einen Saal passte, ohne auch nur entfernt die Leistung eines heutigen
Smartphones zu erreichen, gab es keine grafischen Benutzeroberflächen.

Der einzige Weg, mit Computern zu kommunizieren, bestand zunächst aus
Stapeln von Lochkarten. Und dann gab es Text. Texteingabe, Textausgabe.
Diesem Zeitalter ist das Backbone des Internets nach wie vor verhaftet,
und masochistisch angehauchte Nerds mögen das.

Heute, in einer Welt des ubiquitären Computers, ist der Anwender von
grafischen Oberflächen verwöhnt. Auch die sind quasi morgen schon von
gestern, die Epoche der sprachgesteuerten Assistenten ist angebrochen,
die das gleiche Problem noch mal verschärfen dürften:

Die Mensch-Maschine-Interaktion gestaltet sich auf eine Weise, die es
dem Menschen erschwert, sich gedanklich auf das eigentliche Problem zu
konzentrieren, das er mit dem Computer zu lösen versucht. Ergebnis: Er gibt
die Verantwortung ab, er wird passiv und erduldet alles, und vergisst, dass
jeder Computer letztendlich von irgendwem gesteuert wird, nämlich von den
Entwicklern des Codes. Diese steuern mithin nicht nur den Computer, sondern
implizit auch den Benutzer.

Viel mentale Energie muss er verschwenden, um durch abstrakte multi-
hierarchische Optionswelten zu navigieren. Das wird allerdings in Kauf
genommen, denn der scheinbare Gewinn ist: vom Computer geführt zu werden.

Der Computer macht es unnötig das Problem zu verstehen, das er lösen
soll. Das ist vom Anwender wie auch von der Industrie gewünscht, und
vom Staat in Kauf genommen, auch er profitiert davon. Umso größer sind
halt die Probleme, die der nächste großflächige Stromausfall aufwerfen
wird.

Um etwas wirklich zu lernen – "Deep Learning" für das biologisch realisierte
Gehirn – empfiehlt sich die harte Tour:

Textbasierte Schnittstellen fördern das Verständnis des eigentlich zu
lösenden Problems, während sie die "Trial and Error"-Strategie nicht so
sehr gut unterstützen. Dennnoch bleibt sie möglich.

So habe ich den Sompyler entwickelt: einen Synthesizer, der
Text liest und entsprechend seiner Interpretation dieser Textdaten Klänge
fabriziert. Die Klänge schreibt er als PCM-Samples in eine Audiodatei.
PCM-Samplewerte sind nichts anderes als Zahlenwerte, die die Spannung an
einer virtuellen Lautsprechermembran zu jedem Zeitpunkt kodieren.

Es ist weder ein topmoderner PC zu beschaffen, auf dem eine aufgeblähte
Digital Audio Workstation mit geringer Latenz betrieben werden kann. Noch
wird MIDI-Hardware vorausgesetzt. Allerdings gilt: Je schneller die Hardware 
arbeitet und je mehr Prozessorkerne zur Verfügung stehen, um so weniger
Geduld braucht es.

Andere Projekte wie CSound und SuperCollider verfolgen ähnliche Ziele.
Diese Systeme sind im Gegensatz zum Sompyler echtzeittauglich, für den diese
meist im Kontext der Musikproduktion als selbstverständlich aufgefasste
Anforderung bewusst ausgeklammert wurde. Dafür ist Sompyler auf Ebene des
Quelltextes verständlicher. Zumindest für mich.

Da es auf dem Markt so viel bessere Programme und Geräte gibt, stellt sich
selbstverständlich die Frage, warum ich mit so etwas um die Ecke zu komme,
und zwar einige Jahrzehnte zu spät.

Die Antwort ist erstens, weil es dazumal noch kein Python gab – genauso
wenig wie mich selber. Zweitens, um des Lernens willen. Da ich mir
Musikproduktion eben auf nerdige Art beibringen wollte, erwog ich die
Entwicklung eines eigenen Systems bzw. einer eigenen Sprache und bin so
unabhängiger von den Entscheidungen Dritter, die meine Art und Weise Musik
zu machen, beeinflussen würden. Heute finde ich, dass ich eine gute
Entscheidung getroffen habe.


Was ist Sompyler, was nicht?
----------------------------

Sompyler ist also keine richtige Digital Audio Workstation. Sompyler ist
auch nicht recht vergleichbar mit den Apps, die es hundertfach in Stores
herunterzuladen gibt, um Musikproduktion zum Kinderspiel zu machen,
und damit Musik insgesamt billig im positiven wie im negativen Sinn.

Sompyler als neues Projekt soll der Musikkultur eine andere Richtung geben,
weg vom reinen Konsum, hin zur intellektuellen Wiederschließung der Bauweise
von Musik für alle (Musiktheorie zum praktischen Ausprobieren, ohne ein
Instrument beherrschen zu müssen), die einen simplen Texteditor bedienen
können, hin zur Produktion mit kleinem Footprint:

Musik, die es nicht nur als Audioversion gibt, sondern auch ihr Quelltext
in einem offenen Format zu erstellen und nachher beliebig veränderbar ist.
Diese Art von Musik ist teuer in Geduld, Arbeitszeit, Rechenkapazität oder
in Geld, und das ist gut so. Man kann einfache Musik damit in einer Stunde
fabrizieren, die halt dementsprechend klingt. Oder man investiert mehr, und
bekommt entsprechend mehr.

PovRay ist das 3D-Graphik-Äquivalent zu Sompyler. Das war auch schon 20 Jahre
vorher da und hat zu Sompyler mit inspiriert.

Musik als Programm für Ohren. Wie es zu einer früheren Zeit nicht Kleidung
zu kaufen gab, sondern Strickanleitungen. Wie eine ganze Open-Source-Ökonomie
um Software (genauer, Linux) entstand. Wie auch immer jemand an diesen Text
kam – ohne Open Source wäre dies undenkbar gewesen auf so vielen Ebenen. 

Sompyler ist ein vergleichsweise kleines Programm, das Text-Dateien liest
und eine Audiodatei schreibt, die mit einem beliebigen Player abgespielt
werden kann. Die Eingabeseite kann auch aus nur einer Textdatei bestehen.
Diese enthält dann die Klangbeschreibungen der einzelnen
Stimmen auf der gedachten Bühne. Referenziert eine Stimme keine
eingebettete Klangbeschreibung, braucht sie stattdessen eine Pfadangabe
zur Instrumentdatei (`*.spli`), die diese Beschreibung enthält.

Sompyler hat wenige Abhängigkeiten zu Fremdcode, der ebenfalls installiert
sein muss, damit das Programm funktionieren kann.


Python
------

Sompyler ist überwiegend in Python implementiert, eine Sprache, die zu den
sogenannten interpretierten Sprachen gehört, und womit Software für sehr viele Anwendungszwecke im großen wie im kleinen Maßstab realisiert werden kann.
Sompyler diente mir als Projekt ursprünglich auch dazu, Python zu lernen,
da ich das für ein anderes, berufliches Projekt benötigte.

Eine interpretierte Sprache, auch Skriptsprache genannt, wurde ursprünglich
von einem Interpreterprogramm Zeile für Zeile ausgeführt. Heute ist der
Unterschied zwischen interpretierten und kompilierten Sprachen praktisch nicht
mehr relevant. Er besteht darin, das zur Ausführung eines Programms in einer interpretierten Sprache der Interpreter installiert sein muss, also streng
genommen auch als Abhängigkeit zählt. Eine kompilierte Sprache ist ohne den
"Compiler" lauffähig, allerdings oft nicht ohne eine bestimmte "Bibliothek"
von vielen vorkompilierten Codes.

Daher kommt auch der Name "Sompyler", eine Zusammenziehung von "Sound" und
"Compiler". Ohne die Zusammenziehung hätte "SoundCompiler" dem Nicht-wirklich-
Konkurrenten "SuperCollider" sehr geähnelt.

SuperCollider ist ebenfalls  eine Sprache, womit man Musik machen kann.
Sie ist zwar sicher leistungsfähiger, kann zudem Echtzeit.
Aber ihre Nähe zu Programmiersprachen ist offensichtlich, was mir nicht gefällt.
Sompyler Score soll sich stärker an musikalischen Konzepten orientieren und
verzichtet dafür auf Konstrukten wie bedingte Ausführung und Schleifen.
Sie ist also nicht imperativ, sondern deklarativ. Die Sprache kennt aber
in einigen Kontexten so etwas wie Variablen. Zum Beispiel können Motive,
kleinste musikalisch sinntragende Einheiten mit melodischen, harmonischen
und/oder rhythmischen Kennzeichen zur Wiedererkennung, auf diese Art an
einer Stelle definiert und an beliebigen anderen nur referenziert werden,
um sie zu verwenden.


YAML
----

Eine Komponente, auf die Sompyler angewiesen ist, ist ein YAML-Parser.
Darauf möchte ich näher eingehen, um verständlich zu machen, was nicht
Sompyler ist, sondern seine Abhängigkeiten.

"YAML" steht für "YAML ain't Another Markup Language" – auf deutsch:
YAML ist keine weitere Auszeichnungssprache – und das stimmt sogar, da es
sich genaugenommen um eine Serialisierungssprache für Daten hierarchischer
Struktur handelt.

So eine Sprache definiert nur eine Syntax, die Bedeutung ihrer "Token"
lässt sie offen. YAML spezifiziert etwa, was Anführungszeichen
bedeuten, wo sie nötig sind; dass der negativen Wahrheitswert nicht nur mit
`False` ausgedrückt werden kann, sondern auch mit `NO`, dass man somit eben
Anführungszeichen braucht, wenn man die Länderkennung von Norwegen meint,
und so weiter. An dieser Stelle sei auf die einschlägigen Stellen im
Internet verwiesen, um zu lernen, wie man Daten mit YAML kodiert.
Alternativ kann der Sompyler übrigens mit JSON gefüttert werden, denn JSON
ist zugleich auch YAML, mit ein paar seltenen, keinesfalls für Sompyler
relevanten Einschränkungen

YAML ist recht flexibel. Zu flexibel für manche. So wird ein YAML-Parser entsprechend der Spezifikation der Sprache folgende Zeichenfolge

    `piano: { 3: [ { pitch: "C4", length: 2 } ] }`

genauso interpretieren wie den Text

```
# Kommentare werden mit "#" eingeleitet und vom YAML-Parser ignoriert
piano: # Wir definieren, was die Stimme "piano" in diesem Takt spielen soll
  3:   # Die angegebene Note nach 3 Ticks ab Taktbeginn anschlagen
    pitch: C4 # Tonhöhe: mittleres C
    length: 2 # Tondauer: 2 Ticks
```

An den Sompyler wird der Parser in beiden Fällen sinngemäß zurückgeben:

```
...
key = "piano"
value = dict({
  int(3): list([
    dict({
      str("pitch"): str("C4"),
      str("length"): int(2),
    })
  ])
})
... # Kommentare werden wie gesagt nicht durchgereicht
```

Die weitere Verarbeitung dieser Datenstruktur ist nicht mehr Sache des
Parsers, der ist nur für den Eingabetext zuständig. Es ist der Sompyler,
der dafür sorgt, dass an der richtigen Stelle das mittlere C eines Klaviers
erklingt, oder was auch immer die "piano" zugeordnete Klangbeschreibung
bestimmt.

Der Benutzer kann übrigens beliebige Sammlungen von Klangbeschreibungen als
Verzeichnisse in `lib/instruments` eintragen und in allen Score-Dateien
direkt nutzen. Dort, unterhalb des Verzeichnisses, in dem Sompyler
installiert wurde, wird Sompyler nach einer Klangbeschreibung suchen, wenn
ein Pfad angegeben wurde.

Wenn ein Klang nicht stimmt, liegt das wahrscheinlicher an einer
fehlerhaften Klangbeschreibung als an Sompyler selbst. Das zu Sompyler
mitgelieferte Klavier kann übrigens mit dem Pfad "dev/piano" mit einer
Stimme verknüpft werden. Allein, es mag noch nicht so gut wie ein Klavier
klingen. Das liegt an der Klangbeschreibung, an der ich nicht sorgfältig
genug gearbeitet habe, andere Dinge waren mir zwischenzeitlich wichtiger
geworden; daher heißt das Verzeichnis ja auch `dev` und nicht
`as-good-as-acoustic-instruments`. ;-)

Syntaxfehler in den Dateien werden mit Exception-Stacktraces quittiert.
Diese zu entschlüsseln, gehört zum Lernen dazu. Dabei lerne ich auch oft,
was für Konsequenzen eine leichtfertige Detailentscheidung beim Coden haben
kann und muss das Programm korrigieren. Endet ein Stacktrace
mit einer Exception aus dem Kontext des YAML-Parsings (YAMLError), hat der
Fehler nichts mit dem Sompyler zu tun. In dem Fall sei zur Lösung des
Problems auch auf die YAML-Spezifikation verwiesen.


Wie kann Sompyler ausprobiert werden?
-------------------------------------

### A) Lokale Installation

Sompyler kann einerseits lokal installiert werden. Der Code steht unter
Open-Source-Lizenz frei auf Gitlab zur Verfügung: https://gitlab.com/flowdy/Sompyler.

### B) Online Demo

Exklusiv diejenigen, die akzeptieren und die Implikationen verstehen, dass
bei diesem nichtgewerblichen, nicht-öffentlichen, zur Vermeidung
installationsbezogener Supportanfragen angebotenen Testservice keinerlei
Support, Rechte oder Garantien jedweder Art bestehen (z.B. Datenschutz),
haben eine weitere kostenlose Möglichkeit:

Sompyler lässt sich auch [online ausprobieren](https://demo.neusik.de/sompyle). Der Service lässt nur drei Benutzer parallel zu. Neben der maximalen
Anzahl von drei Benutzern gibt es weitere Limits, die verhindern, dass ein
Benutzer das System über Gebühr belastet, so dass es andere nicht testen
können.

Den Anmeldetrick verrät der Entwickler von Sompyler auf nicht-öffentliche
Anfrage.


Wie sieht eine Sompyler-Score-Datei aus?
----------------------------------------

Eine Sompyler-Score-Datei ("score" = engl. für Musikpartitur) kann mit
einem beliebigen Texteditor erstellt und bearbeitet werden. Word
ist kein Texteditor, genauso wenig jedes andere Programm, das
Formatierungen wie fett oder kursiv anbietet. Das gilt für alle Textdateien,
die nicht nur für Menschen, sondern auch für Computer gemacht sind. Computer
können solche Textformatierungen schlecht interpretieren, da sie mehrdeutig
sind.

Die Datei besteht aus mindestens zwei YAML-Dokumenten. 

Außer das erste sind alle YAML-Dokumente kennzeichnet durch drei
Bindestriche am Zeilenanfang, ohne Leerzeichen dazwischen. Hinter dem
Bindestrichtripel ist gleich eine neue Zeile anzufangen.

Die wichtigste Angabe im ersten Dokument ist `stage: {...}`: Hier werden
die Stimmen spezifiziert, die im Musikstück erklingen sollen. Was diese
Stimmen in den einzelnen Takten tun, dazu sind die folgenden YAML-Dokumente
da.

Über `stage` können beliebige weitere Angaben zum Stück im Format
`Feld: Feldwert` gemacht werden. Etwa `Title: Alle meine Entchen` oder
`Creator: (Traditional)`. Sompyler ignoriert diese Angaben.

Außerdem können Klangbeschreibungen eingebettet werden.
Die einzelnen Klangbeschreibungen sind jeweils einzurücken und mit
`instrument name:` (nicht eingerückt) einzuleiten. Statt "name" bitte eine
beliebige alphanumerische eindeutige Zeichenfolge angeben. Der Name wird
statt eines Pfades zu einer Stimme angegeben, die diese Klangbeschreibung
verwenden soll.

Wird eine Stimme mit einem Pfad verknüpft, der mindestens einen Schrägstrich
 enthalten muss, so wird eine entsprechend benannte Datei mit `*.spls`-
Endung gesucht, und zwar sowohl relativ zur Höhe des Verzeichnisses, das der 
Score-Datei in der Struktur übergeordnet ist und `.use_as_tempdir` enthält.
Das ist entweder eine leere Datei zur Markierung des Verzeichnisses, wo die
temporären Tondateien (möglicherweise mehrfach verwendet) abzulegen sind,
oder das ist ein Link auf ein anderes Verzeichnis zur Ablage der Tondateien. 
Als auch im Installationsverzeichnis von Sompyler, weiters unter
`lib/instruments`. Darin liegt immer auch ein Verzeichnis `dev/` mit
(miserabel klingenden) Instrumenten, die in der Sompyler-Distribution
mitgeliefert sind. 

Statt der YAML-Dokumente kann es im Kopfdokument `measures: {...}` geben,
worin alle Takte als Listenelemente aufgeführt sind. Dies ist nötig, um bei
Bedarf JSON zu unterstützen.


Klangbeschreibungen
-------------------

Die einfachste Klangbeschreibung ist `{}`, also in Verbindung mit Testtönen
und anderem Boilerplate-Code:

```
# test.spls

stage:
    t: 1|1 0 test

instrument test: {}

---
_meta:
    stress_pattern: 2,0,1,0
    ticks_per_minute: 60
    lower_stress_bound: 95
    upper_stress_bound: 100
t: C4 o.o. # oder, wer es verböser mag:
           # t: { 0: C4 1, 2: C4 1 }

```

Dieses Instrument mit einer Stimme verknüpft, der wiederum Noten/Akkorde
zugeordnet sind, wird sie hörbar mit konstanten Sinustönen ohne Einschwing-
und Ausklangphase.

Das Fehlen dieser Phasen veranlassen viele Lautsprecher und Kopfhörer zu
heftigen Klacks. Das bedeutet keinen Qualitätsmangel. Die Hardware versucht
lediglich, dem abrupten Wechsel von "keine Spannung" zu "maximale Spannung"
nachzukommen.

Da sich das nicht besonders gut anhört, fügen wir eine auch genannte Attack-
und Releasephase hinzu:

```
instruments:
    A: 0.25:1,1
    R: 0.25:1;1,0
```

Bei der Attackphase wurde der Startwert `0;` weggelassen, da er implizit
ist. Anstelle `1` ist als y-Angabe der Koordinaten auch eine andere Zahl
möglich, hier aber wenig sinnvoll, da keine anderen y-Werte mit von der
Partie sind. Die y-Werte werden letztendlich alle zu einem Bruchteil von 1
normiert, das heißt, der höchste der angegebenen Werte wird zu eins, die
anderen zu einem proportionalen Bruchteil davon.

Alle Koordinaten werden natürlich interpoliert. Die Attackphase ist eine
Viertelsekunde lang. Beträgt die Sampling-Rate 44.100 Abtastungen pro
Sekunde, sind also 11.025 Abtastungen zu berechnen, wobei alle Werte
logarithmisch sind. Zur Hälfte, also bei Abtastung Nummer 5.512 ist der
Attack also nur um 50dB angewachsen, genauer von -100dBFS auf -50dBFS
(0dBFS repräsentiert die maximale Lautstärke der Stimme, die auch von
anderen Parametern, wie ihrer Position im gedachten Raum abhängt.)

Der genaue Verlauf kann visuell dargestellt werden durchfolgenden Befehl:

`scripts/render-single-sound -S "0.25:1,1"`

Der Befehl sollte an dieser Stelle auch mit mehr als einem Punkt ausprobiert
werden:

`scripts/render-single-sound -S "0.25:1,1;2,1"`

`scripts/render-single-sound -S "0.25:1,8;2,9"`

Warum heißt der Parameter dieses Programms `-S`, der äquivalent ist zu
`--sustain`? Weil nur der auch allein stehen kann. Es gibt auch `-A`
bzw. `--attack`, und `-R` bzw. `--release`. Diese können nur zusammen
verwendet werden.

Soll unser Sinuston auch einen inneren Verlauf haben? Traditionelle
Synthesizer unterscheiden zwischen Decay und Sustain. Da die Grenze nicht
eindeutig ist in Zeiten, in denen ein Computer mal eben eine Bézierverlauf
berechnen kann und keine Stunden dazu braucht, gibt es kein Decay. Seine
Aufgabe des für das Klangbild eines Instruments so wichtigen ersten
Abschwungs vom Maximum übernimmt entweder der Attack, wenn die Dauer fix
sein soll, andernfalls vom Sustain.

(wird fortgesetzt)
