Chained notes syntax
====================

When I translated Beethoven's Moonlight sonata, it was more and more cumbersome to identify and name
each and every note and to quantify the offset for every note, too. Once I have completed the first
movement I decided to develop a more concise syntax that allows for just identifying the notes of the
main beat of a measure, and then I would only encode the intervals. This accords to the way a pianist
reads notes.


Explanation
-----------

After or instead of the length and/or intensity indication to a note that is identified by its name, you
can append a chain of near-by notes without explicit offsets.

A chain of notes includes notes that all differ in their offsets. You cannot define notes to trigger at
the same offset in a chain.

So, instead of

    0: C4 2
    4: E4 2
    6: G4 2

you can write in one line:

    0: C4 o_..o+4_ o+3_

Every character of a chain has a special meaning so type chains with attention:

 * `<space>` be free to use space before a letter.
   Have in mind though, space starts a new repeatable cluster, see `*n`
 * `o` note without custom attributes, always defined by default.
 * `a` note with custom attributes that you have defined in an attributes
   section "a: {...}" in the voice properties or in the measure.
   In the latter case, that definitions are only scoped to the measure it
   belongs to.
 * `<any other letter>` instead of "a" you can use all lower- and uppercase
   letters of unicode, even with diacritics. Choose it wise so it works as
   a mnemonic and you do not have to look it up every now and then.
 * `:label`, chainable, following a letter, can be specified like an article
   to add the indicated properties to that defined to the latter. Properties
   defined for a subsequent label overwrite those with the same name defined
   by the predecessor. An additional ':' in front includes all extensions of
   the last note.
 * ';', if followed by another letter w/o extension(s), separates notes with
   same offset. Each note can have another pitch shift, but other appended
   properties appended apply equally to all notes.
   If the semicolon is followed by one or more spaces, separates parallel
   chains to same base note, each starting with offset and any pitch shift
   reset to 0.
   Please note if followed by neither a letter nor a space, but a number
   the semicolon has a varied meaning, i.e. overlength – see below
 * `+` after L (letter with or without extension[s], let's refer to it by "L"
   from now on): transpose note higher by one semitone
 * `-` after L transpose note lower by one semitone
 * `=` resets note shift, so "o=" is always the pitch of the initially
   indicated base note.
 * `_` after L add a tick to the length. Unless following a letter or another
   `_`, represents the recently used letter. It is implied and must be omitted
   before `=`, `+` or `-`. So, write `_+` as `+`, and lengthening `_` follow.
   `_+` actually yields two notes.
 * `.` pause a tick
 * `<number>` after `+`/`-` and `.`/`_` repeats that sign. It does not matter
   if you use a number or repeat the sign explicitly.

   * `o..` or `o.2` single-tick note before double-tick pause
   * `o++` or `o+2` shift note higher by one whole tone
   * `o__` or `o_2` note with three ticks length. ` _2` is illegal, but `__2`
     is allowed, it means note with same letter and extensions, if any,
     with three ticks lengths.

 * `*n` repeat n times the chain of notes that start from the space before
   (cluster). Caveat: A trill is `o=o++*n`, a semitone sequence is `oo+*n`,
   the equal sign is important here. `*n` can be repeated, n=1 (=2 at initial
   position) can be omitted, greater n can be replaced by as many minus one
   further `*` optionally surrounded by spaces for alignment. Example:
   `E4 _ ++** + ++* +` equals `E4 _ ++*3 + +*2 +`, the lydian scale over E4.
 * `,n` add n ticks to the length, but that additional length does not
   advance the offset of the next note.
 * `;n` additional length is implicitely used by consequent notes until it is
   exceeded.

   * `o,3o+o+o+` is the base note that is held 4 ticks, but the next three
     notes are one each.
   * `o;3o+o+o+` base note is 4 ticks, second is 3, third is two, fourth is
     one.
   * `(...)[n][_*|_n]` kind of a subchain, represents multiple subsequent
     notes sharing the space of one tick. Read under "Clusters" for details.

### Clusters

A cluster can have one of two forms. The first `...[*n]` with optional spaces
 around the star if you want to repeat it, but always separated by space at
the front from preceding parts that do not belong to it. Observe that in this
form, the current number of keys to add to the initial pitch will advance
through the repetitions, so you may need to put a `=` to the first note to
reset the transposition to 0, returning to initial pitch each time.

Using the `*n`-form, n=1 can be omitted. n>1 can be replaced by as many
additional `*`, optionally surrounded by spaces.

You can also use parentheses around chain parts. A so-called paren cluster
stuffs the netlengths of included so they sum only to the length of one tick.
By appending a number to the closing parenthesis, you can stuff the same
cluster multiple times into one tick. Expand the cluster(s) length by using
`_` after the closing parenthesis or the appended number. A single `_` will
double the length, `__` or `_2` will triple it and so forth.

Every time a paren cluster is entered, the first note in it will calculate
its pitch distance from the last note in the embedding chain, ignoring any
preceding paren cluster or its repetitions.  So, e.g.,

  * `D4 3 (o+.)3` will be 3 three ticks of D#4, separated by one tick each,
    but
  * `D4 oo++(o+*2)3 * 2` will be pitches D4, E4, trilled F4, F#4, F4, F#4,
    F4, F#4, then E4, F#4, trilled G4, G#4, G4, G#4, G4, G#4 stuffed into
    a length of 6 ticks!

Inside paren clusters other paren clusters can be nested.
Nesting level is only limited by your ability to understand line noise.


### Chaining on the upper level

Notes defined as strings can be chained with

 * `, ` (mind the space), and
 * ` < ` (mind surrounding spaces)

The first one is used for adjacent notes or note chains with different base
notes. The second is used for notes or note chains to be triggered at the
same offset. It differs in meaning from "; " as semicolon does not introduce
another base note, and " < " separates base notes with chains that can differ
in length. "; "-separated chains of different lengths are an error.

Notes without length and chain will get implicitly those of preceding note.

This way of chaining does not apply to notes defined as a mapping that
must contain a `pitch` entry at least. The optional `chain` entry may not
contain either separators.
