from Sompyler import intonation
import pytest

tuner = intonation.Tuner("lib/tones_euro_de+en.t.spyl")


def test_equal_temp_tuner():
    assert pytest.approx(tuner.frequency_of_tone("A3")) == 220
    assert pytest.approx(tuner.frequency_of_tone("C4")) == 261.6256

def test_tuners_mismatch():
    with pytest.raises(intonation.TunersMismatchError):
        voicetuner = intonation.Tuner(tuner,
                intervals="15:1 15:1 128:7 15:1 24:1 15:1 15:1 128:7 15:1 24:1 15:1 24:1"
            )

def test_just_intervals():
    derived_tuner = intonation.Tuner(tuner,
        intervals="15:1 15:1 128:7 15:1 24:1 15:1 15:1 128:7 15:1 24:1 15:1 128:7"
    )
    dtft = derived_tuner.frequency_of_tone
    assert pytest.approx(dtft("C#4") / dtft("C4")) == 16/15
    assert pytest.approx(dtft("D4") / dtft("C4")) == 9/8
    assert pytest.approx(dtft("D#4") / dtft("C4")) == 6/5
    assert pytest.approx(dtft("E4") / dtft("C4")) == 5/4
    assert pytest.approx(dtft("F4") / dtft("C4")) == 4/3
    assert pytest.approx(dtft("F#4") / dtft("C4")) == 64/45
    assert pytest.approx(dtft("G4") / dtft("C4")) == 3/2
    assert pytest.approx(dtft("G#4") / dtft("C4")) == 8/5
    assert pytest.approx(dtft("A4") / dtft("C4")) == 5/3
    assert pytest.approx(dtft("A#4") / dtft("C4")) == 16/9
    assert pytest.approx(dtft("B4") / dtft("C4")) == 15/8
    assert pytest.approx(dtft("C5") / dtft("C4")) == 2

def test_just_intervals_retuned_by_pos1():
    derived_tuner = intonation.Tuner(tuner,
        intervals="15:1 15:1 128:7 15:1 24:1 15:1 15:1 128:7 15:1 24:1 15:1 128:7"
    )
    dtft = derived_tuner.frequency_of_tone
    # import pdb; pdb.set_trace()
    tuner.retune(1)
    assert pytest.approx(dtft("D4") / dtft("C#4")) == 16/15
    assert pytest.approx(dtft("D#4") / dtft("C#4")) == 9/8
    assert pytest.approx(dtft("E4") / dtft("C#4")) == 6/5
    assert pytest.approx(dtft("F4") / dtft("C#4")) == 5/4
    assert pytest.approx(dtft("F#4") / dtft("C#4")) == 4/3
    assert pytest.approx(dtft("G4") / dtft("C#4")) == 64/45
    assert pytest.approx(dtft("G#4") / dtft("C#4")) == 3/2
    assert pytest.approx(dtft("A4") / dtft("C#4")) == 8/5
    assert pytest.approx(dtft("A#4") / dtft("C#4")) == 5/3
    assert pytest.approx(dtft("B4") / dtft("C#4")) == 16/9
    assert pytest.approx(dtft("C5") / dtft("C#4")) == 15/8
    assert pytest.approx(dtft("C#5") / dtft("C#4")) == 2
    tuner.retune(-1)

def test_just_intervals_retuned_by_neg1():
    derived_tuner = intonation.Tuner(tuner,
        intervals="15:1 15:1 128:7 15:1 24:1 15:1 15:1 128:7 15:1 24:1 15:1 128:7"
    )
    dtft = derived_tuner.frequency_of_tone
    tuner.retune(-1)
    assert pytest.approx(dtft("C4") / dtft("B3")) == 16/15
    assert pytest.approx(dtft("C#4") / dtft("B3")) == 9/8
    assert pytest.approx(dtft("D4") / dtft("B3")) == 6/5
    assert pytest.approx(dtft("D#4") / dtft("B3")) == 5/4
    assert pytest.approx(dtft("E4") / dtft("B3")) == 4/3
    assert pytest.approx(dtft("F4") / dtft("B3")) == 64/45
    assert pytest.approx(dtft("F#4") / dtft("B3")) == 3/2
    assert pytest.approx(dtft("G4") / dtft("B3")) == 8/5
    assert pytest.approx(dtft("G#4") / dtft("B3")) == 5/3
    assert pytest.approx(dtft("A4") / dtft("B3")) == 16/9
    assert pytest.approx(dtft("A#4") / dtft("B3")) == 15/8
    assert pytest.approx(dtft("B4") / dtft("B3")) == 2
    tuner.retune(1)

def test_just_intervals_retuned_by_pos2():
    derived_tuner = intonation.Tuner(tuner,
        intervals="15:1 15:1 128:7 15:1 24:1 15:1 15:1 128:7 15:1 24:1 15:1 128:7"
    )
    dtft = derived_tuner.frequency_of_tone
    # import pdb; pdb.set_trace()
    tuner.retune(2)
    assert pytest.approx(dtft("D#4") / dtft("D4")) == 16/15
    assert pytest.approx(dtft("E4") / dtft("D4")) == 9/8
    assert pytest.approx(dtft("F4") / dtft("D4")) == 6/5
    assert pytest.approx(dtft("F#4") / dtft("D4")) == 5/4
    assert pytest.approx(dtft("G4") / dtft("D4")) == 4/3
    assert pytest.approx(dtft("G#4") / dtft("D4")) == 64/45
    assert pytest.approx(dtft("A4") / dtft("D4")) == 3/2
    assert pytest.approx(dtft("A#4") / dtft("D4")) == 8/5
    assert pytest.approx(dtft("B4") / dtft("D4")) == 5/3
    assert pytest.approx(dtft("C5") / dtft("D4")) == 16/9
    assert pytest.approx(dtft("C#5") / dtft("D4")) == 15/8
    assert pytest.approx(dtft("D5") / dtft("D4")) == 2
    tuner.retune(-2)

def _obsolete_code(derived_tuner):
    print(
        """
        NOTE: For flat signed keys C# is correctly named Db etc., according to frequency of Db!
        Technically C# and Db are still the same key, it is only tuned appropriately
        Music-theoretically this is questionable, justly-tuned C# and Db need to be handled
        differently, but to reserve an extra key when you can retune the scale with a brease
        of ease and name the tones as you want, it would be needlessly cumbersome.
        And do not forget that all scales in the circle of fifth can be paired, i.e. a scale
        with n flat or sharp signs is the very same as the scale with 12-n sharp or flat signs.
        """
    )
    TONICS = ("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B")
    for _ in range(12):
        print("-------- retune to tonic {} ------------".format(TONICS[_]))
        print("A4/D4: " + str(derived_tuner.frequency_of_tone("A4") / derived_tuner.frequency_of_tone("D4")))
        print("G4/C4: " + str(derived_tuner.frequency_of_tone("G4") / derived_tuner.frequency_of_tone("C4")))
        for tone in TONICS:
            print(
                "{}: {:.3f} {}: {:.3f} eq{}: {:.3f} {}: {:.3f} eq{}: {:.3f} {}: {:.3f}".format(
                tone+"3", derived_tuner.frequency_of_tone(tone+"3"),
                tone+"4", derived_tuner.frequency_of_tone(tone+"4"),
                tone+"4", tuner.frequency_of_tone(tone+"4"),
                tone+"5", derived_tuner.frequency_of_tone(tone+"5"),
                tone+"5", tuner.frequency_of_tone(tone+"5"),
                tone+"6", derived_tuner.frequency_of_tone(tone+"6")
            ))
            if tone == "G":
                print("  G3 (from C chord): " + str(derived_tuner.frequency_of_tone("C4&-1k")))
                print("  G4 (from C chord): " + str(derived_tuner.frequency_of_tone("C4&+2k")))
                print("  G5 (from C chord): " + str(derived_tuner.frequency_of_tone("C4&+5k")))
    
        # tuner is retuned in a cumulative way, i.e. by one twelve times:
        tuner.retune(1)

_obsolete_code(intonation.Tuner(tuner,
        intervals="15:1 15:1 128:7 15:1 24:1 15:1 15:1 128:7 15:1 24:1 15:1 128:7"
    ))

