On the lib/instruments hierarchy
================================

Only dev/ collection is official.

Be welcome to make other subdirectories that are not part of the official repository.
Hence any other subdirectories are ignored by git. Feel free to have git govern your
own collection. Make sure you initialize git for a specific collection inside its
subdirectory.
