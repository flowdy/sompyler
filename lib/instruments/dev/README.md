About the instrument definitions in dev/
========================================

It is the natural creator's bias that their work is that of a genius. It has to prove.

As the piano is my favourite instrument, I accepted the challenge and made an inquiry (roughly)
in a german pianist's board, and guess ... it did not prove. The supposedly experienced pianists
were not at all convinced, they even said how I dare to even consider asking them about this noise,
that I should really start listening to the sound of a real mechanical piano, which by the way
I do regularly in weekly piano lessons, with my hands on the keys of a real concert-size grand(!) piano.

Ruder wordings, by which someone tried to make my work ridiculous, lead me to doubt that this are neutral
judgements, maybe there speaks unadmitted principle that piano sound must only
be emitted by a thing that looks like a piano. Supposed creator's bias against supposed critics' bias
is not very likely to make for a fruitful discussion, alas, it was worth the try. Said discussion
actually animated me to do a number of iterations, resulting in a gradual rise of
quality (in my ears) and I would wonder about how I could dare to upload my first drafts.
In this sense it *was* fruitful at best, their constant denegation notwithstanding.

Well, I don't know. So, well, just let me level down the expectations:

You find herein instrument definitions named after conventional instruments, because I was finally able
to get my own ears to tell me sincerely that my piano.spli actually sounds like a piano, my string.spli
actually sounds like a cello or a violin, my trumpet.spli actually sounds like a trumpet or a
trombone. Be that just because my ears were finally annoyed and bugged out of my bias, but after all
the file names are also chosen as a reference to the source of inspiration and the studied literature.

Be welcome to achieve better, if you like. Be sure you put new instruments to dev/ when you consider
making a pull request. No other subdirectories, neither `*.spli` files directly in lib/instruments are
accepted so we avoid naming conflicts with user-specific collections in lib/instruments which might
be git repositories of their own. For more information read the policy for [lib/instruments hierarchy](../README.md).

