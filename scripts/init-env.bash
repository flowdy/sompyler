#!/bin/bash
# Sompyler environment initialization in Bash-compatible shells
export PROJECT_BASE=$(cd $(dirname $BASH_SOURCE)/..; pwd)
exec {out}<<'EOF'
printf "Starting %s in SOMPYLER development environment.\n" $SHELL
source /etc/bash.bashrc
source ~/.bashrc
source $PROJECT_BASE/venv/bin/activate
export PATH=$PATH:$PROJECT_BASE/scripts
alias cdp="cd $PROJECT_BASE"
alias ss=sompyle
alias s=$PROJECT_BASE/scripts/sompyle
alias r=render-single-sound
alias p=probe-single-sound
PS1='SOMPYLER(\u@\h:\w) \$ '
HISTFILE=$PROJECT_BASE/.bash_history 
cdp
EOF

exec $SHELL --rcfile /dev/fd/"$out" # implies exit(), what follows ignored.
