#!/usr/bin/env python
import script_preamble
import numpy, re, soundfile, matplotlib, sys, os, argparse, tempfile
import matplotlib.pyplot as plt
from yaml import safe_load as yamload
from Sompyler.shapereverb import Room
from Sompyler.synthesizer import SAMPLING_RATE, normalize_amplitude
from Sompyler.audiofile import save

argp = argparse.ArgumentParser(
        prog="add-reverb",
        description="Enrich a free-field recorded audio file "
                    "with reverberation",
)

argp.add_argument(
    '-r', '--room-specfile',
    metavar="FILE",
    default=None,
    type=argparse.FileType('r')
)

argp.add_argument(
    '--levels',
    default=None,
    metavar='L|R_OR_BOTH',
)

argp.add_argument(
    '--delays',
    default=None,
    metavar='L|R_OR_BOTH',
)

argp.add_argument(
    '--jitter',
    default=None,
    metavar='L|R_OR_BOTH',
)

argp.add_argument(
    '--freq-lanes',
    default=None,
    nargs='+',
    metavar='SHAPE',
)

argp.add_argument(
    '--border',
    default=None,
    metavar='SHAPE',
)

argp.add_argument(
    '--deldiffs', '--delay-diffs',
    default=None,
    metavar="L|R_OR_BOTH",
)

def pos(panorama_position):
    m = re.fullmatch(r"(\d+):(\d+),(\d+)", panorama_position)
    if m:
        panorama_position = int(m.group(1)), int(m.group(2))
        intensity = 1 / (int(m.group(3)) + 1)
        return (panorama_position, intensity)
    else:
        raise argparse.ArgumentError(
            "Panorama position syntax: $left:$right,$distance"
        )

argp.add_argument(
    'panorama_position',
    type=pos,
)

argp.add_argument(
    'infile',
    type=argparse.FileType('rb'),
    default=None,
)

argp.add_argument(
    'outfile',
    default=None,
    type=argparse.FileType('wb'),
)

args = argp.parse_args()

if bool(args.outfile) ^ bool(args.infile):
    if args.infile:
        args.outfile = tempfile.TemporaryFile()
        def save_outfile(samples):
            filename = args.infile.name
            audioformat = filename.rsplit(".")[1]
            save(samples, args.outfile, format=audioformat)
            args.outfile.seek(0)
            args.infile.close()
            outfile = open(filename, 'wb')
            chunk = None
            while chunk != '':
                chunk = args.outfile.read(1024**2)
                outfile.write(chunk)
            outfile.close()
    else:
        raise argparse.ArgumentError(
            "outfile parameter given, but infile missing"
        )
else:
    def save_outfile(samples):
        return save(samples, args.outfile)


if args.room_specfile:
    room_spec = yamload(args.room_specfile)
else:
    room_spec = {}

for arg in ('levels', 'delays', 'jitter', 'border', 'deldiffs'):
    argvalue = getattr(args, arg)
    if argvalue is None: continue
    room_spec[arg] = argvalue

if args.infile.name.endswith(".npy"):
    soundata = numpy.load(args.infile)
else:
    soundata, SAMPLING_RATE = soundfile.read(args.infile)
    soundata = normalize_amplitude(
            soundata.sum(axis=1) if len(soundata.shape) > 1
                                 else soundata
        )

room = Room(**room_spec)
position = room.position(*args.panorama_position)

try:
    _ = os.get_terminal_size()
except OSError:
    pass
else:
   fig, (ax1, ax2) = plt.subplots(ncols=2)
   ax1.plot(range(len(position.left_reverb[1])), position.left_reverb[1])
   ax2.plot(range(len(position.right_reverb[1])), position.right_reverb[1])
   print(f"Reverb costs: {position.virtual_samples_count}")
   fig.show()
   try:
       input("Press Enter to continue or Strg-D to abort ...")
   except EOFError:
       print()
       exit()

save_outfile(position.apply_reverb_to_sound(soundata))
