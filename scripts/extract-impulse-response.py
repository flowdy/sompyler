import sys, numpy, soundfile
from matplotlib import pyplot as plt

_, wavefile, begin_offset, end_offset = sys.argv

def output_graph(*vstack, title=None):
    fig, axs = plt.subplots(len(vstack))
    if title is not None:
        fig.suptitle(title)
    for i, data in enumerate(vstack):
        if isinstance(data, tuple):
            data, alpha = data
            alpha = (100 + 20 * numpy.log10(numpy.where(alpha == 0, 1e-5, alpha))) / 100
            breakpoint()
            alpha = numpy.asarray([(0,0,1,a) for a in alpha])
            axs[i].bar(range(data.size), data, color=alpha)
        else:
            axs[i].plot(range(data.size), data)
    plt.plot() # savefig("/tmp/approx_reverb.png", dpi=1200)
    plt.close()


sdata, ssr = soundfile.read(wavefile)
sdata = sdata[int(begin_offset):int(end_offset)].mean(axis=1)
# output_graph(sdata)
sdiff = numpy.abs(numpy.diff(sdata))
sdiff = sdiff[ sdiff.nonzero() ]

# output_graph(sdiff)
win_integ_diff = numpy.zeros(sdiff.size)
window_size = numpy.argmax(sdiff)
# breakpoint()
for i in range(win_integ_diff.size):
    left_end = i - window_size // 2
    right_end = i + window_size // 2
    if left_end < 0:
        right_end -= left_end
        left_end = 0
    if right_end > win_integ_diff.size:
        left_end -= right_end - win_integ_diff.size
        right_end = win_integ_diff.size
    win_integ_diff[i] += sdiff[i] / numpy.average(sdiff[ left_end : right_end ])
win_integ_diff -= numpy.min(win_integ_diff)
win_integ_diff /= numpy.max(win_integ_diff)
output_graph(sdata,
        (100 + 20 * numpy.log10(sdiff), win_integ_diff), title=f"Reverb approximation of {wavefile}")


# win_integ_diff2 = numpy.diff(win_integ_diff)
# zero_points = []
# for i in range(win_integ_diff2.size-1):
#     d0, d1 = win_integ_diff2[i:i+2]
#     skip = abs(d1 - d0)
#     if d1 < 0 and skip > abs(d1):
#         zero_points.append( (i, skip) )
# sorted_zero_points = numpy.array(list(
#     i[0] for i in reversed(sorted(zero_points, key=lambda n: n[1]))
# )[:int(upper_levels)])
# sdiff_mask = numpy.zeros(sdiff.size)
# sdiff_mask[ sorted_zero_points ] = 100 + 20 * numpy.log10(sdiff[ sorted_zero_points ])
