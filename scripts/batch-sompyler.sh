#!/bin/bash
# 
# .spls to .wav+.mp3 batch processing and conversion
# ==================================================
#
# This file can be executed as wrapper for SOMPYLER_DIRECTORY/scripts/sompyle
# that also autodetects and reuses any cache directory for the given spls
# file, so tones for notes that have not been changed or added are not
# re-rendered.
#
# This is only a script for convenience; scripts/sompyle is usable without.
#
# PREREQUISITES:
# --------------
#
# Make sure you have installed following tools:
#
# - GNU find or any other find supporting -samefile operator
# - lame: LAME Encoder
#
#
# INSTALLATION:
# -------------
#
# Symlink this file from a /bin-directory in your $PATH, or copy and adjust
# it according to your needs.
#
#
# USAGE:
# ------
#
# $name_of_link [OPTIONS --] FILE [FILE2 FILE3 ... FILEn]
#
# The OPTIONS are simply passed to the wrapped script. Execute
# scripts/sompyle without parameters to get a list and usage help.
#
# Do not forget the final "--" that means that following parameters are the
# files to process. Files indicated without .spls suffix are regarded as
# having one and they must be contained in $SOMPYLER_DIRECTORY/scores/.
#
# 
# CAUTION:
# --------
#
# This script is not quite portable, yet.

# Replace if you have copied and customized scripts/sompyle-wrapper.sh:
homedir=$(cd "$(dirname $(readlink $0 || printf "%s" $0))"/..; pwd)
# Use temporary directory as is stored in ${TMPDIR} environment variable
# If not set, assume /tmp.
export tempdir=${TMPDIR:-/tmp}

# Supported by shells with array support
declare -a FILES ARGS

IFS=":"
score_paths="${SOMPYLER_SCORE_PATH:-${homedir}/scores}"

for i in "$@"; do
    if [ $i == "--" ]; then
       ARGS_ONLY=0
    elif [ "$ARGS_ONLY" == 1 ] || [[ "$i" == -* ]]; then
       ARGS+=("$i")
       ARGS_ONLY=1
    else
       FILES+=("$i")
    fi
    done

for score in "${FILES[@]}"; do
    if [[ "${score}" != *.spls ]] && [[ ! "${score}" =~ ^[./] ]]; then
	# unless $score has file ending .spls or starts with . or /
	score_name="${score%.*}"
	output_ext="${score##*.}"
	if [[ "${score_name}" == "${output_ext}" ]]; then
             output_ext="wav"
	else
	     score="${score_name}"
	fi
	for scores_dir in "${score_paths[@]}"; do
	     test_dir="${scores_dir}/$score"
             if [ -d "$test_dir" ]
	     	then score_path="${test_dir}/score"
	        else score_path="${test_dir}.spls"
	     fi
	     if [ -e "${score_path}" ]; then break; else score_path=; fi
        done
	if [ "x$score_path" == "x" ]; then
	    >&2 printf "%s Cannot resolve to existing file: %s.spls (Searched %s)\n" $xy "${score}" "${score_paths[*]}"
	    continue
	fi
    else
	score_path="${score}"
    fi
    out="${tempdir}/$(basename "${score%.spls}").${output_ext}"
    "${homedir}/scripts/sompyle" "${score_path}" "${out}" "${ARGS[@]}"
    if [ -n "${lame}" ]; then
	    lame -V4 "$out" && rm "$out"
    fi
done
